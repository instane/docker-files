#docker-files

Set of docker files for creating LEMP with some web application like symfony.

## Requirements

docker with docker-compose

For installation refer to [guide for your OS](https://store.docker.com/search?type=edition&offering=community)

## Description

There are several containers:

* db - MariaDB database server
* redis - Redis in-memory data structure store
* nginx - NGINX web server
* php-fpm - PHP FastCGI Process Manager
* workspace - PHP CLI with composer, NodeJS with NPM and yarn

## Usage

### Pre

Make your .env file or use predefined:

``cp .env.example .env``

Start all containers 

``docker-compose up``

Now you can access web server. By default it is symfony.localhost:8080

### Main

Start all containers and daemonize

``docker-compose up -d``

Start specified container

``docker-compose up workspace``

Start all containers and rebuild

``docker-compose up --build``

Build containers

``docker-compose build``

Build specified container

``docker-compose build workspace``

Execute command in running container

``docker-compose exec workspace sh``

### Post

#### Debugging with PhpStorm

First of all ensure that your php-fpm container built with PHP_FPM_INSTALL_XDEBUG=true

Go to Settings -> Languages & Frameworks -> PHP -> Servers

If you use default .env, then configure all as in image below.

![phpstorm-server-configuration](doc/img/phpstorm-server-configuration.png)

After this configuration go to Tools -> DBGp proxy -> configuration

If you use default .env, then configure IDE key and PORT as in image below and host as your local machine address.

![phpstorm-dbgp-configuration](doc/img/phpstorm-dbgp-configuration.png)

After this configurations you can set breakpoints and see debug info

![phpstorm-xdebug-catch](doc/img/phpstorm-xdebug-catch.png)