# REQUIRED SECTION
ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
include $(ROOT_DIR)/.mk-lib/common.mk
include .env
export $(shell sed 's/=.*//' .env)
# END OF REQUIRED SECTION

.PHONY: help dependencies up start stop restart status ps clean

dependencies: check-dependencies ## Check dependencies

build-up: ## Build and start all or c=<name> containers in foreground
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) up --build $(c)

build-upd: ## Build and start all or c=<name> containers in background
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) up --build -d $(c)

up: ## Start all or c=<name> containers in foreground
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) up $(c)

start: ## Start all or c=<name> containers in background
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) up -d $(c)

stop: ## Stop all or c=<name> containers
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) stop $(c)

restart: ## Restart all or c=<name> containers
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) stop $(c)
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) up $(c) -d

build: ## Build all or c=<name> containers
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) build $(c)

exec: ## Exec command in c=<name> container
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) exec $(c) $(filter-out $@,$(MAKECMDGOALS))

# TODO: extend run command for all workspace related
run: ## Run command cmd=<command> in c=<name> container
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) run --rm $(c) $(filter-out $@,$(MAKECMDGOALS))

composer: ## Run composer command in workspace container
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) run --rm $(WORKSPACE_HOST) \
	 composer $(filter-out $@,$(MAKECMDGOALS))

console: ## Run console command in workspace container
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) run --rm $(WORKSPACE_HOST) \
	 php bin/console $(filter-out $@,$(MAKECMDGOALS))

migrate: ## Run migrate console command in workspace container
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) run --rm $(WORKSPACE_HOST) \
	 php bin/console doctrine:migrations:migrate

migrate-diff: ## Run console command in workspace container
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) run --rm $(WORKSPACE_HOST) \
	 php bin/console doctrine:migrations:diff

mycli: ## Run mycli in workspace container
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) run --rm $(WORKSPACE_HOST) \
	 mycli -h $(MYSQL_HOST) -u $(MYSQL_USER) -p $(MYSQL_PASSWORD) -D $(MYSQL_DATABASE)

php-cs-fixer: ## Run php-cs-fixer in workspace container
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) run --rm $(WORKSPACE_HOST) \
	 composer exec php-cs-fixer fix -v

workspace: ## Run workspace container in foreground interactive mode
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) run --rm $(WORKSPACE_HOST) sh

status: ## Show status of containers
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) ps

ps: status ## Alias of status

clean: ## Clean all data
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) down

down: clean ## Alias of clean
