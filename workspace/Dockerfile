ARG PHP_VERSION=${PHP_VERSION}

FROM php:${PHP_VERSION}-cli-alpine

RUN apk update \
    && apk add nodejs nodejs-npm yarn

RUN apk add curl libmemcached-dev libjpeg jpeg-dev \
    libpng-dev freetype-dev openssl-dev libmcrypt-dev

RUN docker-php-ext-install pdo_mysql \
    && docker-php-ext-configure gd \
    --enable-gd-native-ttf \
    --with-jpeg-dir=/usr/lib \
    --with-freetype-dir=/usr/include/freetype2 && \
    docker-php-ext-install gd

RUN curl --insecure https://getcomposer.org/composer.phar -o /usr/bin/composer && chmod +x /usr/bin/composer

RUN composer global require hirak/prestissimo

ARG INSTALL_XDEBUG=false

RUN if [ ${INSTALL_XDEBUG} = true ]; then \
    apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS; \
    pecl install xdebug; \
    docker-php-ext-enable xdebug; \
    apk del .phpize-deps \
;fi

ARG INSTALL_ZIP=false

RUN if [ ${INSTALL_ZIP} = true ]; then \
    apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS; \
    apk add libzip-dev zip; \
    docker-php-ext-configure zip --with-libzip; \
    docker-php-ext-install -j$(nproc) zip; \
    apk del .phpize-deps \
;fi

ARG INSTALL_MYCLI=false

RUN if [ ${INSTALL_MYCLI} = true ]; then \
    apk add python py-pip; \
    apk add --no-cache --virtual .mycli-deps gcc python-dev linux-headers musl-dev libffi-dev openssl-dev; \
    pip install mycli; \
    apk del .mycli-deps \
;fi

WORKDIR /var/www/symfony
